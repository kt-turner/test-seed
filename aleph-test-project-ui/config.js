System.config({
  baseURL: "/",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system"
    ]
  },
  paths: {
    "app/*": "dist/app/*",
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*",
    "npm-redbee:*": "jspm_packages/npm-redbee/*",
    "bower:*": "jspm_packages/bower/*"
  },

  map: {
    "angular": "github:angular/bower-angular@1.4.5",
    "angular-bootstrap": "bower:angular-bootstrap@0.13.4",
    "angular-encode-uri": "bower:angular-encode-uri@1.0.0",
    "angular-material": "github:angular/bower-material@0.10.1",
    "angular-messages": "bower:angular-messages@1.4.9",
    "angular-resource": "github:angular/bower-angular-resource@1.4.5",
    "angular-route": "github:angular/bower-angular-route@1.4.5",
    "angular-ui-router": "github:angular-ui/ui-router@0.2.15",
    "babel": "npm:babel-core@5.8.22",
    "babel-runtime": "npm:babel-runtime@5.8.20",
    "bootstrap-sweetalert": "bower:bootstrap-sweetalert@0.4.5",
    "clean-css": "npm:clean-css@3.4.1",
    "core-js": "npm:core-js@1.2.6",
    "css": "github:systemjs/plugin-css@0.1.15",
    "jquery": "github:components/jquery@2.1.4",
    "module-crud-ui": "npm-redbee:module-crud-ui@1.2.5-beta",
    "module-layout-ui": "npm-redbee:module-layout-ui@1.2.6-beta",
    "module-security-ui": "npm-redbee:module-security-ui@1.0.0",
    "remarkable-bootstrap-notify": "bower:remarkable-bootstrap-notify@3.1.3",
    "bower:angular-bootstrap@0.13.4": {
      "angular": "bower:angular@1.4.5"
    },
    "bower:angular-cookies@1.4.5": {
      "angular": "bower:angular@1.4.5"
    },
    "bower:angular-encode-uri@1.0.0": {
      "angular": "bower:angular@1.4.5"
    },
    "bower:angular-growl-v2@0.7.9": {
      "angular": "bower:angular@1.4.5",
      "css": "github:systemjs/plugin-css@0.1.15"
    },
    "bower:angular-messages@1.4.9": {
      "angular": "bower:angular@1.4.9"
    },
    "bower:bootstrap-sweetalert@0.4.5": {
      "css": "github:systemjs/plugin-css@0.1.15"
    },
    "bower:bootstrap@3.3.6": {
      "jquery": "bower:jquery@2.2.1"
    },
    "bower:material-design-iconic-font@2.1.2": {
      "css": "github:systemjs/plugin-css@0.1.15"
    },
    "bower:remarkable-bootstrap-notify@3.1.3": {
      "bootstrap": "bower:bootstrap@3.3.6",
      "jquery": "bower:jquery@2.2.1"
    },
    "github:angular-translate/bower-angular-translate-loader-static-files@2.10.0": {
      "angular-translate": "github:angular-translate/bower-angular-translate@2.10.0"
    },
    "github:angular-translate/bower-angular-translate@2.10.0": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular-ui/ui-router@0.2.15": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-animate@1.4.8": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-aria@1.5.0": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-cookies@1.5.0": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-mocks@1.4.4": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-route@1.4.4": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-route@1.4.5": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-angular-sanitize@1.5.0": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:angular/bower-material@0.10.1": {
      "angular": "github:angular/bower-angular@1.4.5",
      "angular-animate": "github:angular/bower-angular-animate@1.4.8",
      "angular-aria": "github:angular/bower-angular-aria@1.5.0",
      "css": "github:systemjs/plugin-css@0.1.15"
    },
    "github:chieffancypants/angular-loading-bar@0.8.0": {
      "angular": "github:angular/bower-angular@1.4.5",
      "css": "github:systemjs/plugin-css@0.1.15"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-buffer@0.1.0": {
      "buffer": "npm:buffer@3.6.0"
    },
    "github:jspm/nodelibs-events@0.1.1": {
      "events": "npm:events@1.0.2"
    },
    "github:jspm/nodelibs-http@1.7.1": {
      "Base64": "npm:Base64@0.2.1",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "stream": "github:jspm/nodelibs-stream@0.1.0",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "github:jspm/nodelibs-https@0.1.0": {
      "https-browserify": "npm:https-browserify@0.0.0"
    },
    "github:jspm/nodelibs-os@0.1.0": {
      "os-browserify": "npm:os-browserify@0.1.2"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-stream@0.1.0": {
      "stream-browserify": "npm:stream-browserify@1.0.0"
    },
    "github:jspm/nodelibs-url@0.1.0": {
      "url": "npm:url@0.10.3"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "npm-redbee:module-crud-ui@1.2.5-beta": {
      "angular": "github:angular/bower-angular@1.4.5",
      "angular-mocks": "github:angular/bower-angular-mocks@1.4.4",
      "angular-resource": "github:angular/bower-angular-resource@1.4.4",
      "angular-route": "github:angular/bower-angular-route@1.4.4",
      "angular-ui-router": "github:angular-ui/ui-router@0.2.15"
    },
    "npm-redbee:module-layout-ui@1.2.6-beta": {
      "angular": "github:angular/bower-angular@1.4.5",
      "angular-bootstrap": "bower:angular-bootstrap@0.13.4",
      "angular-cookies": "github:angular/bower-angular-cookies@1.5.0",
      "angular-growl-v2": "bower:angular-growl-v2@0.7.9",
      "angular-loading-bar": "github:chieffancypants/angular-loading-bar@0.8.0",
      "angular-material": "github:angular/bower-material@0.10.1",
      "angular-sanitize": "github:angular/bower-angular-sanitize@1.5.0",
      "angular-translate": "github:angular-translate/bower-angular-translate@2.10.0",
      "angular-translate-loader-static-files": "github:angular-translate/bower-angular-translate-loader-static-files@2.10.0",
      "angular-ui-router": "github:angular-ui/ui-router@0.2.15",
      "angular-utils-ui-breadcrumbs": "bower:angular-utils-ui-breadcrumbs@0.2.2",
      "bootstrap-sweetalert": "bower:bootstrap-sweetalert@0.4.5",
      "clean-css": "npm:clean-css@3.4.1",
      "css": "github:systemjs/plugin-css@0.1.15",
      "jquery": "github:components/jquery@2.1.4",
      "material-design-iconic-font": "bower:material-design-iconic-font@2.1.2"
    },
    "npm-redbee:module-security-ui@1.0.0": {
      "angular": "bower:angular@1.4.5",
      "angular-cookies": "bower:angular-cookies@1.4.5",
      "angular-uuid": "npm:angular-uuid@0.0.2"
    },
    "npm:amdefine@1.0.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "module": "github:jspm/nodelibs-module@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:angular-uuid@0.0.2": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.20": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:buffer@3.6.0": {
      "base64-js": "npm:base64-js@0.0.8",
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "ieee754": "npm:ieee754@1.1.6",
      "isarray": "npm:isarray@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:clean-css@3.4.1": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "commander": "npm:commander@2.8.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "http": "github:jspm/nodelibs-http@1.7.1",
      "https": "github:jspm/nodelibs-https@0.1.0",
      "os": "github:jspm/nodelibs-os@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "source-map": "npm:source-map@0.4.4",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:commander@2.8.1": {
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "graceful-readlink": "npm:graceful-readlink@1.0.1",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.6": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-util-is@1.0.2": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:graceful-readlink@1.0.1": {
      "fs": "github:jspm/nodelibs-fs@0.1.2"
    },
    "npm:https-browserify@0.0.0": {
      "http": "github:jspm/nodelibs-http@1.7.1"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:os-browserify@0.1.2": {
      "os": "github:jspm/nodelibs-os@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:punycode@1.3.2": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:readable-stream@1.1.13": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "core-util-is": "npm:core-util-is@1.0.2",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "isarray": "npm:isarray@0.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "stream-browserify": "npm:stream-browserify@1.0.0",
      "string_decoder": "npm:string_decoder@0.10.31"
    },
    "npm:source-map@0.4.4": {
      "amdefine": "npm:amdefine@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:stream-browserify@1.0.0": {
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "readable-stream": "npm:readable-stream@1.1.13"
    },
    "npm:string_decoder@0.10.31": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:url@0.10.3": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "punycode": "npm:punycode@1.3.2",
      "querystring": "npm:querystring@0.2.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
