package com.turner.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Component;

/**
 * Created by biandra on 11/02/15.
 */
@Component
public class DateConverter {

    //TODO: in Commons
    public static final String ISOFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String ISOFORMAT_TIME = "HH:mm:ss";

    public java.sql.Date convertStringToSqlDate(String sDate){
        return Optional.ofNullable(sDate)
                .map(nonNullValue ->
                {
                    try {
                        return new java.sql.Date(new SimpleDateFormat(ISOFORMAT).parse(nonNullValue).getTime());
                    } catch (ParseException e) {
                        return null;
                    }

                }).orElse(null);
    }

    public java.sql.Timestamp convertToTimeStamp(String sDate, final String format){

        return Optional.ofNullable( sDate )
                .map(nonNullValue -> {

                    try {

                        java.util.Date tempDate = new Date(new SimpleDateFormat(format).parse(nonNullValue).getTime());

                        return new java.sql.Timestamp(tempDate.getTime());

                    } catch (ParseException e){

                        return null;
                    }

                }).orElse(null);
    }

    public java.sql.Timestamp convertToTimeStamp(java.util.Date date){

        return Optional.ofNullable( date )
                .map(nonNullValue ->
                        new java.sql.Timestamp(nonNullValue.getTime())
                ).orElse(null);
    }

    public String convertToFormat( java.sql.Timestamp timeStamp, final String format){

        return Optional.ofNullable(timeStamp)
                .map(nonNullValue -> new SimpleDateFormat(format).format(nonNullValue))
                .orElse(null);

    }

    public String convertToFormat( java.sql.Timestamp timeStamp){

        return Optional.ofNullable(timeStamp)
                .map(nonNullValue -> new SimpleDateFormat(ISOFORMAT).format(nonNullValue))
                .orElse(null);

    }


    public java.sql.Date convertStringTimeToSqlDate(String sDate){
        return Optional.ofNullable(sDate)
                .map(nonNullValue ->
                {
                    try {

                        return new java.sql.Date(new SimpleDateFormat(ISOFORMAT_TIME).parse(nonNullValue).getTime());

                    } catch (ParseException e) {
                        return null;
                    }

                }).orElse(null);
    }

    public String convert(java.sql.Date sqlDate){
        return Optional.ofNullable(sqlDate)
                .map(nonNullValue -> new SimpleDateFormat(ISOFORMAT).format(nonNullValue))
                .orElse(null);
    }

    public String convertTime(java.sql.Date sqlDate){
        return Optional.ofNullable(sqlDate)
                .map(nonNullValue -> new SimpleDateFormat(ISOFORMAT_TIME).format(nonNullValue))
                .orElse(null);
    }


    public String convertTime(String sDate, String format) {
        return Optional.ofNullable(sDate)
                .map(nonNullValue ->
                {
                    try {
                        Date date =  new SimpleDateFormat(format).parse(nonNullValue);
                        return new SimpleDateFormat(ISOFORMAT_TIME).format(date);
                    } catch (ParseException e) {
                        return null;
                    }
                }).orElse(null);

    }

    public java.sql.Date convertSqlDate(java.util.Date utilDate){
        return new java.sql.Date(utilDate.getTime());
    }
}