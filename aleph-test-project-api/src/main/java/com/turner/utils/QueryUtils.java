package com.turner.utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mbritez on 4/01/16.
 */
public class QueryUtils {

    public static final String WHERE = " WHERE ";
    public static final String JOINING = ", ";
    public static final String AND = " AND ";
    public static final String DEFAUL_CONDITION = " = ?";
    public static final String IS_NULL = " IS NULL ";
    public static final String OR = " OR ";
    private List<String> arguments = new ArrayList<>();
    private List<Object> argumentsValues = new ArrayList<>();
    private List<ConditionArgument> whereArguments = new ArrayList<>();
    private List<Object> whereArgumentsValues = new ArrayList<>();
    private List<QueryUtils> subQuerys = new ArrayList<>();

    public void addParam(String key, Object value){
        if(isValid(key,value)) {
            this.arguments.add(key);
            this.argumentsValues.add(value);
        }
    }

    public void addParam(Object value){
        if(isValid(value)){
            this.argumentsValues.add(value);
        }
    }

    public void addParams(Map<String, Object> params){
        this.arguments.addAll(params.keySet());
        this.argumentsValues.addAll(params.values());
    }

    public void addWhereParam(String key, Object value){
        if(isValid(key,value)){
            this.whereArguments.add(new ConditionArgument(key, DEFAUL_CONDITION));
            this.whereArgumentsValues.add(value);
        }
    }

    public void addWhereParam(String key, Object value, String condition){
        if(isValid(key,value)){
            this.whereArguments.add(new ConditionArgument(key, condition));
            this.whereArgumentsValues.add(value);
        }
    }

    public void addWhereParam(String key, List<Object> values, String condition){
        if(isValid(key,values)){
            this.whereArguments.add(new ConditionArgument(key, condition));
            this.whereArgumentsValues.addAll(values);
        }
    }

    public void addWhereParams(Map<String, Object> params){
        params.keySet().stream().forEach(key -> this.whereArguments.add(new ConditionArgument(key, DEFAUL_CONDITION)));
        this.whereArgumentsValues.addAll(params.values());
    }

    public void addWhereOrParam(String key, Object value, String condition){
        if(isValid(key,value)){
            this.whereArguments.add(new ConditionArgument(key, condition, true));
            this.whereArgumentsValues.add(value);
        }

    }

    public void addWhereNotExistingParam(String key) {
        if (isValid(key)){
            this.whereArguments.add(new ConditionArgument(key));
        }
    }

    private Boolean isValid(String key, Object value){
        return isValid(key) && isValid(value);
    }

    private Boolean isValid(Object value){
        return Optional.ofNullable(value).isPresent();
    }


    public List<Object> getArguments(){
        List<Object> result = new ArrayList<>(argumentsValues);
        result.addAll(whereArgumentsValues);
        subQuerys.stream().forEach(queryUtils -> result.addAll(queryUtils.whereArgumentsValues));
        return result;
    }

    public String getKeyArguments(){
        return new StringBuilder()
                .append(arguments
                        .stream()
                        .collect(Collectors.joining(JOINING)))
                        .toString();
    }

    public String getWhereClause(){
        if (whereArguments.isEmpty()){
            return "";
        } else{
            StringBuilder result = new StringBuilder()
                    .append(collect(arguments))
                    .append(WHERE)
                    .append(collectCondition(whereArguments));
            this.subQuerys.stream().forEach(queryUtils -> {
                        result.append(AND + " ( ");
                        result.append(collectCondition(queryUtils.whereArguments));
                        result.append(" ) ");
                    });
            return result.toString();
        }
    }

    private String collectCondition(List<ConditionArgument> params){
        StringBuilder result = new StringBuilder();
        result.append(params
                .stream()
                .filter(conditionArgument -> !conditionArgument.isOrCondition())
                .map(conditionArgument -> getArgumentWithCondition(conditionArgument))
                .collect(Collectors.joining(AND)));
        params
                .stream()
                .filter(conditionArgument -> conditionArgument.isOrCondition())
                .forEach(conditionArgument -> {
                    result.append(OR);
                    result.append(getArgumentWithCondition(conditionArgument));
                });
        return result.toString();
    }

    private String getArgumentWithCondition(ConditionArgument it) {
        return Optional.ofNullable(it.getCondition()).isPresent() ? it.getArgument() + it.getCondition() : it.getArgument() + IS_NULL;
    }

    private String collect(List<String> params){
        return params
                .stream()
                .map(it -> it + DEFAUL_CONDITION)
                .collect(Collectors.joining(JOINING));
    }

    public void addSubquery(QueryUtils queryUtils) {
        subQuerys.add(queryUtils);
    }
}
