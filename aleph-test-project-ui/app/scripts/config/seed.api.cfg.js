/* jshint node: true */
'use strict';

let SeedApiCfg = () => {

  let apiConfig = ['apiProvider', (apiProvider) => {

    apiProvider.setBaseRoute('/api/v1');

    apiProvider.endpoint('groupcodes')
      .route('/groupcodes/:groupcodeId');
    
  }];

  return apiConfig;
};

export default SeedApiCfg;
