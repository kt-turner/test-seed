/* jshint node: true */
'use strict';
import Pageable from 'module-crud-ui/app/scripts/crud.pageable.js';
import Saveable from 'module-crud-ui/app/scripts/crud.saveable.js';

export default class GroupcodesPageableCtrl extends Pageable {

  /*@ngInject*/
  constructor($injector ,favGroupcodeService){
    super({
    	injector: $injector,
    	endpoint: 'groupcodes',
    	selectable: {
    		enabled:true,
    		itemKey:'id'		
    	}
    })
    this.favGroupcodeService = favGroupcodeService;
    this.saveable= new Saveable({
    	injector: $injector,
    	endpoint: 'groupcodes',
    	backToState: 'groupcodes2',
    	key: 'groupcodeId'
    })
  }
  
  selectItem(event, groupcode){
	  event.stopPropagation();
	  this.selectable.change(groupcode);
  }
  
  delete(){
	  this.saveable.delete({groupcodeId:this.selectable.itemSelected().id});
  }
}
