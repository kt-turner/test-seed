package com.turner.domain;

import java.util.Collections;
import java.util.Set;

import io.aleph.rest.template.model.AbstractModelEntity;

public class Groupcode extends AbstractModelEntity<String> {

    private String description;
    private String createdOn;

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public Set validate() {
		// TODO Auto-generated method stub
		return Collections.emptySet();
	}

}