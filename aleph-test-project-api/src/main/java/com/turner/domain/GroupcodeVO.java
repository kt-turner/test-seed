package com.turner.domain;

import java.sql.Date;

public class GroupcodeVO {

    private String gcdCodigo;
    private String gcdDescripcion;
    private Date fAlta;

	public String getGcdCodigo() {
        return gcdCodigo;
    }

    public void setGcdCodigo(String gcdCodigo) {
        this.gcdCodigo = gcdCodigo;
    }

    public String getGcdDescripcion() {
        return gcdDescripcion;
    }

    public void setGcdDescripcion(String gcdDescripcion) {
        this.gcdDescripcion = gcdDescripcion;
    }
    
    public Date getFAlta() {
		return fAlta;
	}

	public void setFAlta(Date fecAlta) {
		this.fAlta = fecAlta;
	}
}