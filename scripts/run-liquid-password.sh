#!/bin/bash

ENV=$1
SRC_FOLDER=$2
DEST_FOLDER=$3
DB=$4
USER=$5
PASS=$6

USAGE="Usage: <env> <from> <to> <db> <user> <password>"

usage() {
    if [ "$#" -ne 6 ]; then
        echo $USAGE;
        exit 1;
    fi
}

copy() {
    ssh -t -t $1 "mkdir -p $3"
    scp -r $2/* $1:$3
}

copyScript() {
    scp -r $2/../scripts/liquid-password.sh $1:$3
}

run() {
    ssh -t -t $1 "sh $2/liquid-password.sh $3 $4 $5"
}

#Begin copy
usage $ENV $SRC_FOLDER $DEST_FOLDER $DB $USER $PASS;
echo "Attempt to copy files";
copy $ENV $SRC_FOLDER $DEST_FOLDER;
echo "Attempt to copy script";
copyScript $ENV $SRC_FOLDER $DEST_FOLDER;
echo "Attempt to run liquid script";
run $ENV $DEST_FOLDER $DB $USER $PASS;
echo "Script finished successfully";