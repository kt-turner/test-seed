package com.turner.v1.resources.groupcodes.adapter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.turner.domain.Groupcode;
import com.turner.utils.QueryUtils;
import com.turner.v1.resources.groupcodes.adapter.converter.GroupcodeConverter;
import com.turner.v1.resources.groupcodes.adapter.repository.GroupcodeRepository;

import io.aleph.rest.template.exception.RedbeeResourceNotFoundException;

@Service
public class GroupcodeService {

	public static final String GCD_DESCRIPCION = "GCD_DESCRIPCION";
	public static final String GCD_CODIGO = "GCD_CODIGO";
	
	private GroupcodeRepository repository;
	private GroupcodeConverter converter;	

	@Autowired
	public GroupcodeService(GroupcodeRepository repository, GroupcodeConverter converter) {
		this.repository = repository;
		this.converter = converter;
	}

	public Page<Groupcode> list() {
		return converter.convert(this.repository.findAll(new ArrayList(), "", null, null));
	}
	
	public Groupcode get(String id) {
		return converter.convert(this.repository.findOne(id).get());
	}
	
    @Transactional(rollbackFor = RuntimeException.class)
    public Optional<String> create(Groupcode groupcode) {
    	QueryUtils queryUtils = new QueryUtils();
        queryUtils.addParam(GCD_CODIGO, Optional.ofNullable(groupcode.getId()).orElseThrow(RedbeeResourceNotFoundException::new));
        queryUtils.addParam(GCD_DESCRIPCION, Optional.ofNullable(groupcode.getDescription()).orElseThrow(RedbeeResourceNotFoundException::new));
        this.repository.save(groupcode.getId(), queryUtils.getKeyArguments(), queryUtils.getArguments());
        return Optional.of(groupcode.getId());
    }
        
    @Transactional(rollbackFor=RuntimeException.class)
    public void update(Groupcode groupcode){
    	QueryUtils queryUtils = new QueryUtils();
    	Optional.ofNullable(groupcode.getDescription()).ifPresent(notnullable -> queryUtils.addParam(GCD_DESCRIPCION, groupcode.getDescription()));
    	queryUtils.addWhereParam(GCD_CODIGO, Optional.ofNullable(groupcode.getId()).orElseThrow(RedbeeResourceNotFoundException::new));
        this.repository.update(queryUtils.getArguments(), queryUtils.getWhereClause());
    }  
    
    @Transactional(rollbackFor = RuntimeException.class)
    public void delete(String groupcodeId) {
        List<Object> argumentsValues = new ArrayList<>();
        argumentsValues.add(Optional.ofNullable(groupcodeId).orElseThrow(RedbeeResourceNotFoundException::new));
        this.repository.delete(argumentsValues);
    }
    
    public Boolean exists(String id){
        QueryUtils queryUtils = new QueryUtils();
        queryUtils.addParam(id);
        return repository.exists(queryUtils.getArguments().toArray());
    }
    
}