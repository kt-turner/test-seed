/* jshint node: true */
'use strict';

import GroupcodesCtrl from '../../groupcodes/scripts/controllers/seed.groupcodes.ctrl';
import GroupdetailCtrl from '../../groupcodes/scripts/controllers/seed.groupcode-detail.ctrl';
import GroupdetailSaveableCtrl from '../../groupcodes/scripts/controllers/seed.groupcode-detail.saveable.ctrl';
import NewGroupcodeCtrl from '../../groupcodes/scripts/controllers/seed.new-groupcode.ctrl';
import GroupcodesPageableCtrl from '../../groupcodes/scripts/controllers/seed.groupcodes.pageable.ctrl';

let SeedRoutes = () => {

  let routerConfig = [
    '$stateProvider',
    '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) => {

      $urlRouterProvider.otherwise(($injector)=> {
        $injector.get('$state').go('groupcodes2');
      });

      $stateProvider
        .state('groupcodes', {
          url: '/groupcodes',
          data: { displayName : 'Group' },
          views: {
            '': {
              controller: GroupcodesCtrl,
              controllerAs: 'vm',
              templateUrl: 'groupcodes/views/seed.groupcodes.html'
            }
          }
        }
     )
     
       .state('groupcodes2', {
          url: '/groupcodes2',
          data: { displayName : 'Group' },
          views: {
            '': {
              controller: GroupcodesPageableCtrl,
              controllerAs: 'vm',
              templateUrl: 'groupcodes/views/seed.groupcodes.pageable.html'
            }
          }
        }
     )
        .state('groupcodedetail', {
          url: '/groupcodes/:id',
          data: { displayName : 'Groupdetail' },
          views: {
            '': {
              controller: GroupdetailCtrl,
              controllerAs: 'vm',
              templateUrl: 'groupcodes/views/seed.groupcode-detail.html'
            }
          }
        }
     )
     
      .state('groupcodedetail2', {
          url: '/groupcodes2/:groupcodeId',
          data: { displayName : 'Groupdetail' },
          views: {
            '': {
              controller: GroupdetailSaveableCtrl,
              controllerAs: 'vm',
              templateUrl: 'groupcodes/views/seed.groupcode-detail.saveable.html'
            }
          }
        }
     )
      .state('newgroupcode', {
          url: '/groupcodes/new',
          data: { displayName : 'newGroupcode' },
          views: {
            '': {
              controller: NewGroupcodeCtrl,
              controllerAs: 'vm',
              templateUrl: 'groupcodes/views/seed.new-groupcode.html'
            }
          }
        }
     )
        
  }];

  return routerConfig;

};

export default SeedRoutes;
