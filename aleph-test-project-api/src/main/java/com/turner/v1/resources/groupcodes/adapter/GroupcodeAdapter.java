package com.turner.v1.resources.groupcodes.adapter;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.turner.domain.Groupcode;
import com.turner.v1.resources.groupcodes.adapter.service.GroupcodeService;

import io.aleph.rest.template.exception.AlephBusinessException;
import io.aleph.rest.template.exception.AlephException;
import io.aleph.rest.template.exception.RedbeeBussinesException;
import io.aleph.rest.template.exception.RedbeePLException;
import io.aleph.rest.template.repository.adapter.RedbeeRepositoryAdapter;
import io.aleph.rest.template.request.RedbeeBodyRequest;
import io.aleph.rest.template.request.RedbeeRequest;

/**
 * Created by LFavaro on 18/05/2016.
 */
@Component
public class GroupcodeAdapter implements RedbeeRepositoryAdapter<Groupcode, String> {

    private GroupcodeService service;
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupcodeAdapter.class);

    @Autowired
    public GroupcodeAdapter(GroupcodeService service) {
        this.service = service;
    }

    @Override
    public Page<Groupcode> list(RedbeeRequest request){
        try{
            LOGGER.info("Getting list of groupcodes");
            Page<Groupcode> result = service.list();
            LOGGER.info("Success");
            return result;
        }catch(AlephException ex){
            LOGGER.error("Business Error trying to list Groupcode" , ex);
            throw new RedbeePLException(ex.getCode(),ex.getMessage());
        }
    }

    @Override
    public Optional<Groupcode> get(RedbeeRequest request){
        try{
            LOGGER.info("Getting groupcode");
            Groupcode result = service.get(request.getId("id", String.class));
            LOGGER.info("Success");
            return Optional.ofNullable(result);
        }catch(AlephException ex){
            LOGGER.error("Business Error trying to get Groupcode", ex);
            throw new RedbeePLException(ex.getCode(),ex.getMessage());
        }
    }

    @Override
    public Optional<String> create(RedbeeBodyRequest<Groupcode> request) {
        String id = request.getBody().getId();
        try{
            LOGGER.info(String.format("Create Groupcode with id: %s", id));
            this.service.create(request.getBody());
            LOGGER.info(String.format("Successfully creation of Groupcode with id: %s", id));
        } catch (AlephBusinessException e){
            LOGGER.error(String.format("Business Error trying to create Groupcode with id: %s", id) , e);
            throw new RedbeeBussinesException(e.getCode(), e.getMessage());
        } catch (AlephException e){
            LOGGER.error(String.format("Error trying to create Groupcode with id: %s", id), e);
            throw new RedbeePLException(e.getCode(), e.getMessage());
        }
        return Optional.of(id);
    }
    
    @Override
    public void update(RedbeeBodyRequest<Groupcode> request) {
        String id = request.getBody().getId();
        try{
            LOGGER.info(String.format("Update Groupcode with id: %s", id));
            this.service.update(request.getBody());
            LOGGER.info(String.format("Successfully update of Groupcode with id: %s", id));
        } catch (AlephBusinessException e){
            LOGGER.error(String.format("Business Error trying to update Groupcode with id: %s", id) , e);
            throw new RedbeeBussinesException(e.getCode(), e.getMessage());
        } catch (AlephException e){
            LOGGER.error(String.format("Error trying to update Groupcode with id: %s", id), e);
            throw new RedbeePLException(e.getCode(), e.getMessage());
        }
    }    
    
    @Override
    public void delete(RedbeeRequest redbeeRequest) {
        String groupcodeId = redbeeRequest.getId("id", String.class);
        try {
            LOGGER.info(String.format("Deleting Groupcode with id: %s", groupcodeId));
            service.delete(groupcodeId);
            LOGGER.info(String.format("Deleted Groupcode with id: %s and all their configurations", groupcodeId));
        } catch (AlephBusinessException e){
            LOGGER.error(String.format("Business Error trying to delete Groupcode with id: %s", groupcodeId), e);
            throw new RedbeeBussinesException(e.getCode(), e.getMessage());
        } catch (AlephException e){
            LOGGER.error(String.format("Business Error trying to delete Groupcode with id: %s", groupcodeId) , e);
            throw new RedbeePLException(e.getCode(), e.getMessage());
        }
    }
    
    @Override
    public boolean exists(RedbeeRequest request) {        
    	return this.service.exists(request.getId("id",String.class));
    }
    
}