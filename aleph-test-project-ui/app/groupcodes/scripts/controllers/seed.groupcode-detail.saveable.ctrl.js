/* jshint node: true */
'use strict';

import Saveable from 'module-crud-ui/app/scripts/crud.saveable.js';

export default class GroupdetailSaveableCtrl extends Saveable {
  
	/*@ngInject*/
	constructor($injector, favGroupcodeService){
		super({
			injector: $injector,
			endpoint: 'groupcodes',
			key: 'groupcodeId',
			backToState: 'groupcodes2'
		});
		this.favGroupcodeService = favGroupcodeService;
	}
	 
}
