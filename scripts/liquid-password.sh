#!/bin/bash

export LIQUIBASE_HOME=/home/desa/liquibase

export CLASSPATH=custom_lib/ojdbc6.jar:$CLASSPATH

$LIQUIBASE_HOME/liquibase --driver=oracle.jdbc.driver.OracleDriver \
     --classpath=$CLASSPATH \
     --url="$1" \
     --changeLogFile=/home/desa/redbee-plcallgen/generated/liquibase-jenkins.xml \
     --username=$2 \
     --password=$3 \
     --logLevel=DEBUG \
     migrate

     #--changeLogFile=/home/desa/redbee-plcallgen/src/main/java/io/redbee/turner/pl/generated/liquibase-jenkins.xml \
