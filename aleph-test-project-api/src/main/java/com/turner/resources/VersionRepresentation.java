package com.turner.resources;

/**
 * Created by biandra on 25/08/15.
 */
public class VersionRepresentation {

    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
