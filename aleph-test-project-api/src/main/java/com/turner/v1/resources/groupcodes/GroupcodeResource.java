package com.turner.v1.resources.groupcodes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turner.domain.Groupcode;
import com.turner.resources.BaseResource;
import com.turner.v1.resources.groupcodes.adapter.GroupcodeAdapter;

import io.aleph.rest.template.RedbeeResource;

/**
 * Created by pablo on 17/03/16.
 */
@RestController(GroupcodeResource.NAME)
@RequestMapping(value = BaseResource.API + BaseResource.V1 + GroupcodeResource.PATH)
public class GroupcodeResource extends RedbeeResource<Groupcode, String> {

    public static final String PATH = BaseResource.PATH + "groupcodes";
    public static final String NAME = BaseResource.V1 + "GROUPCODES_HANDLER";

    @Autowired
    public GroupcodeResource(GroupcodeAdapter theAdapter) {
        super(theAdapter);
    }
}
