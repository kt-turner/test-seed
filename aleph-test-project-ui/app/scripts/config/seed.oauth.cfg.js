/* jshint node: true */
'use strict';

let SeedOAuthCfg = () => {

    let oauthCfg = (OAuthProvider) => {
      OAuthProvider.configure({
          clientId: 'branding',
          revokePath: '/logout'
      });
    };

    return oauthCfg;

};

export default SeedOAuthCfg;
