/* jshint node: true */
'use strict';

import NewGroupcodeCtrl from './seed.new-groupcode.ctrl'

export default class GroupdetailCtrl extends NewGroupcodeCtrl {
  
	/*@ngInject*/
	constructor(api, $state, favGroupcodeService){
		super(api, $state);
		this._execute();
		this.favGroupcodeService = favGroupcodeService;
	}
	
	_execute() {
		this.isEdit=false;
		this.api
			.groupcodes
			.get( {groupcodeId:this.state.params.id} )
			.$promise
			.then(response => {
				this.groupcode=response;}
			)
	}
	
	_delete(){		
		this.api
			.groupcodes
			.remove({groupcodeId:this.state.params.id})
			.$promise
			.then(response => {
				this.state.go('groupcodes')
			});
	}
		
	save(form){
		if(!form.$valid){
			return false
		}
		
		this.api
		.groupcodes
		.update({groupcodeId:this.state.params.id},this.groupcode)
		.$promise
		.then(response => {
			this.isEdit=false;
			this._callbackExecute()
		});
	}  
}
