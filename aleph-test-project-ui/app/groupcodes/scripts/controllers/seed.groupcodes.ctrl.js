/* jshint node: true */
'use strict';

export default class GroupcodesCtrl {

  /*@ngInject*/
  constructor(api,$state,favGroupcodeService){
    this.api = api;
    this.state = $state;
    this.message = 'Hello World';
    this._execute();
    this.favGroupcodeService = favGroupcodeService;
  }
  
  _execute() {
	this.api
		.groupcodes
		.get()
		.$promise
		.then(response => {
			this.groupcodes=response.content;
		}
	)
  }
  
  showgroupcodedetail(groupcodeid){
	this.state.go('groupcodedetail',{id:groupcodeid} ); 
	
  }
}
