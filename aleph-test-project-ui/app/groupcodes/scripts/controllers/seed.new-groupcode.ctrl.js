/* jshint node: true */
'use strict';

export default class NewGroupcodeCtrl {

	/*@ngInject*/
	constructor(api, $state){
		this.api = api;
		this.state = $state;
	}
		
	save(form){
		if(!form.$valid){
			return false
		}
		
		this.api
			.groupcodes
			.save({},this.groupcode)
			.$promise
			.then(response => {
				this._callbackExecute()				
				});
	}

	_callbackExecute() {
		this.state.go('groupcodedetail',{id:this.groupcode.id})
	}  
}