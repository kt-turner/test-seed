'use strict'

export default class FavGroupcodeService{
	
	/*@ngInject*/
	constructor(){
		
		
		
	}
	
	getFav(){
		return this.fav;
	}
	
	setFav(groupcodeFav){
		this.fav = groupcodeFav;
		
	}
	
	isFav(groupcode){
		  if (typeof this.fav === 'undefined'){
			  return false;
		  }
		  	  
		  return this.fav.id === groupcode.id;
	  }
	
}


