package com.turner.v1.resources.groupcodes.adapter

import com.turner.domain.Groupcode
import com.turner.v1.resources.groupcodes.adapter.service.GroupcodeService
import io.aleph.rest.template.exception.AlephException
import io.aleph.rest.template.exception.RedbeePLException
import io.aleph.rest.template.request.RedbeeRequest
import org.springframework.data.domain.Page
import spock.lang.Specification


/**
 * Created by martin on 27/06/16.
 */
class GroupcodeAdapterTest extends Specification{

    GroupcodeAdapter adapter
    GroupcodeService service

    void setup() {
        service = Mock()
        adapter = new GroupcodeAdapter(service)
    }

    def "test de el metodo list"(){
        setup:
        def request = Mock(RedbeeRequest)
        def page = Mock(Page)

        when:
        def result = adapter.list(request)

        then:
        result
        result.equals(page)
        1 * service.list() >> page
    }

    def "test de el metodo list with RedbeePLException"(){
        setup:
        def request = Mock(RedbeeRequest)
        1 * service.list() >> {throw Mock(AlephException)}

        when:
        adapter.list(request)

        then:
        thrown(RedbeePLException)
    }

    def "tesdts del metodo get"(){
        setup:
        def request = Mock(RedbeeRequest)
        def groupcode = Mock(Groupcode)

        1 * request.getId("id",String.class) >> "aId"

        1 * service.get("aId") >> groupcode

        when:
        def result = adapter.get(request)

        then:
        result != null
        result.isPresent()
        result.get().equals(groupcode)
    }

    def "tesdts del metodo ge t"(){
        setup:
        def request = Mock(RedbeeRequest)
        1 * request.getId("id",String.class) >> "aId"
        1 * service.get("aId") >> {throw Mock(AlephException)}

        when:
        adapter.get(request)

        then:
        thrown(RedbeePLException)
    }
}
