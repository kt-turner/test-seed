package com.turner.configuration;

/**
 * Created by pcastelo on 3/3/16.
 */

public enum BrandingAuthorities {

    URL_1("/api/v1/brandingmedias", "POST", "BRAP"),
    URL_2("/api/v1/brandingmedias?*", "GET", "BRCP"),
    URL_3("/api/v1/brandingmedias/*", "PUT", "BRMP"),
    URL_4("/api/v1/brandingmedias/*", "DELETE", "BRDP"),
    URL_5("/api/v1/brandingmedias/*/dynamictexts", "POST", "BRTA"),
    URL_6("/api/v1/brandingmedias/*/dynamictexts", "POST", "BRAM"),
    URL_7("/api/v1/brandingmedias/*/dynamictexts", "GET", "BRTC"),
    URL_8("/api/v1/brandingmedias/*/dynamictexts", "PUT", "BRTM"),
    URL_9("/api/v1/brandingmedias/*/dynamictexts", "PUT", "BRMT"),
    URL_10("/api/v1/brandingmedias/dynamictexts/*", "DELETE", "BRTD"),
    URL_11("/api/v1/brandingmedias/feeds/*", "DELETE", "BRFD"),
    URL_12("/api/v1/brandingmedias/*/configurations", "POST", "BRCA"),
    URL_13("/api/v1/brandingmedias/*/configurations", "GET", "BRCC"),
    URL_14("/api/v1/brandingmedias/*/configurations**", "PUT","BRCM"),
    URL_15("/api/v1/brandingmedias/*/configurations/*/brandingconftext", "POST", "BRMF"),
    URL_16("/api/v1/brandingmedias/*/configurations/*/brandingconftext" , "POST", "BRFM"),
    URL_17("/api/v1/brandingmedias/*/configurations/*/brandingconfmat", "POST", "BRAT"),
    URL_18("/api/v1/brandingmedias/*/configurations/*/brandingconfseg", "POST", "BRAB"),
    URL_19("/api/v1/brandingmedias/*/configurations/*/brandingconftext", "PUT", "BRUF"),
    URL_20("/api/v1/brandingmedias/*/configurations/*/brandingconftext", "PUT", "BRFU"),
    URL_21("/api/v1/brandingmedias/*/configurations/*/brandingconfseg", "PUT", "BRUB"),
    URL_22("/api/v1/brandingmedias/*/configurations/*/brandingconfmat", "PUT", "BRUM"),
    URL_23("/api/v1/brandingmedias/*/patterns", "GET", "BRGP"),
    URL_24("/api/v1/scenetypecontents/", "GET", "BRST"),
    URL_25("/api/v1/processstatuscontents/", "GET", "BRPS"),
    URL_26("/api/v1/usercontents/", "GET", "BRUS"),
    URL_27("/api/v1/positionscontents/", "GET", "BRPO"),
    URL_28("/api/v1/bugstatuscontents/", "GET", "BRBS"),
    URL_29("/api/v1/eventtypecontents/", "GET", "BRET"),
    URL_30("/api/v1/blockcontents/", "GET", "BRBL"),
    URL_31("/api/v1/programcontents/", "GET", "BRGR"),
    URL_32("/api/v1/materialtypecontents/", "GET", "BRT2"),
    URL_33("/api/v1/feeds/", "GET", "BRFE"),
    URL_34("/api/v1/days/", "GET", "BRDA"),
    URL_35("/api/v1/credittypes/", "GET", "BRCT"),
    URL_36("/api/v1/brandingmedias/*/feeds", "GET", "BRFC"),
    URL_37("/api/v1/brandingmedias/*/programs", "GET", "BRM2"),
    URL_38("/api/v1/brandingmedias/*/dynamictexts/current", "GET", "BRDT"),
    URL_39("/api/v1/titletypes/", "GET", "BRTT"),
    URL_40("/api/v1/wildcards/", "GET", "BRWI"),
    URL_41("/api/v1/languages/", "GET", "BRLA"),
    URL_42("/api/v1/brandingmedias/*/configurations/*/fixedtexts", "GET", "BRCF"),
    URL_43("/api/v1/brandingmedias/*/configurations/*/materials", "GET", "BRMA"),
    URL_44("/api/v1/brandingmedias/*/patterns", "GET", "BRP2"),
    URL_45("/api/v1/brandingmedias/*/dynamictexts", "GET", "BRDY"),
    URL_46("/api/v1/brandingmedias/*/dynamictext", "GET", "BRD2"),
    URL_47("/api/v1/brandingmedias/*/programs", "GET", "BRG2"),
    EDIT_PROCESS_STATUS("/someUrl/", "PUT", "BRPE");


    private final String url;
    private final String method;
    private final String role;

    BrandingAuthorities(String url, String method, String role) {
        this.url = url;
        this.method = method;
        this.role = role;
    }

    public String getUrl() {
        return this.url;
    }

    public String getMethod() {
        return this.method;
    }

    public String getRole() {
        return this.role;
    }
}

