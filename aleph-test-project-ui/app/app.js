/*jshint strict:false */
'use strict';

import angular from 'angular';
import 'angular-resource';
import 'angular-ui-router';
import 'angular-material';
import 'angular-messages';

import 'module-crud-ui/app/scripts/crud.module.js';
import 'module-security-ui/app/scripts/sec.module.js';
import 'bower:angular-bootstrap@0.13.4';

import SeedRoutes from './scripts/config/seed.router.cfg';
import SeedOAuthCfg from './scripts/config/seed.oauth.cfg.js';
import SeedApiCfg from './scripts/config/seed.api.cfg.js';

//import '../styles/layout.css!';

import 'module-layout-ui';

import './templates';

import GroupcodeDirective from './groupcodes/scripts/directives/seed.groupcode.dir.js';

import FavGroupcodeService from './groupcodes/scripts/services/seed.fav.groupcode.service.js';

var app = angular.module('seed.app',
  [
    'ui.router',
    'security.module',
    'crud.module',
    'layout.module',
    'ngMaterial',
    'aleph-project-ui-templates',
    'ngMessages'
  ]);

System.import('jquery').then(function () {
  angular.element(document).ready(function () {
    angular.bootstrap(document.body, [app.name], {
      // strictDi: true
    });
  });
});

app.run((OAuthToken, $location) => {

  let token = $location.path().substr(1);

  if(typeof token !== 'undefined' && token !== '' && token.indexOf('access_token')===0 ){
    OAuthToken.setToken(token);
  }
});

app
  .config(SeedOAuthCfg())
  .config(SeedRoutes())
  .config(SeedApiCfg())
  .directive('seedGroupcode',GroupcodeDirective.directiveFactory)
  .service('favGroupcodeService',FavGroupcodeService);

export default app;
