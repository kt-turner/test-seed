package com.turner.v1.resources.groupcodes.adapter.repository;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.turner.domain.GroupcodeVO;

import io.aleph.rest.template.action.RedbeeAction;
import io.aleph.rest.template.annotation.RedbeeQueries;
import io.aleph.rest.template.annotation.RedbeeQuery;
import io.aleph.rest.template.repository.jdbc.JDBCDefaultRedbeeRepository;

/**
 * Created by Marco on 15/01/16.
 */
@Repository
@RedbeeQueries(value = {
		@RedbeeQuery(action = RedbeeAction.LIST, value = "SELECT GCD_CODIGO " 
				+ ", GCD_DESCRIPCION "
				+ ", F_ALTA "
				+ "FROM PRO_GROUP_CODE"),
		@RedbeeQuery(action = RedbeeAction.SHOW, value = "SELECT GCD_CODIGO " 
				+ ", GCD_DESCRIPCION "
				+ ", F_ALTA "
				+ "FROM PRO_GROUP_CODE " + "WHERE GCD_CODIGO = ?"),
		@RedbeeQuery(action = RedbeeAction.SAVE, value = "INSERT INTO PRO_GROUP_CODE "),
		@RedbeeQuery(action = RedbeeAction.UPDATE, value = "UPDATE PRO_GROUP_CODE SET "),
		@RedbeeQuery(action = RedbeeAction.DELETE, value = "DELETE FROM PRO_GROUP_CODE WHERE GCD_CODIGO = ? "),
		@RedbeeQuery(action = RedbeeAction.EXIST, value = "SELECT '1' FROM PRO_GROUP_CODE WHERE GCD_CODIGO = ? ") })

public class GroupcodeRepository extends JDBCDefaultRedbeeRepository<GroupcodeVO, String> {

	@Autowired
	public GroupcodeRepository(DataSource dataSource) {
		super(dataSource);
	}
}