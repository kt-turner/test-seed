package com.turner.v1.resources.groupcodes.adapter.converter;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import com.turner.domain.Groupcode;
import com.turner.domain.GroupcodeVO;
import com.turner.utils.DateConverter;

/**
 * Created by Marco on 15/02/16.
 */
@Component
public class GroupcodeConverter {

	private DateConverter dateConverter;
	
	@Autowired
	public GroupcodeConverter( DateConverter dateConverter ){
		this.dateConverter = dateConverter;
	}
	
    public PageImpl<Groupcode> convert(Page<GroupcodeVO> vos){
       return new PageImpl<>(vos.getContent().stream().map(this::convert).collect(Collectors.toList())); 
    }

    public Groupcode convert(GroupcodeVO vo){
        Groupcode g = new Groupcode();
        g.setId(vo.getGcdCodigo());
        g.setDescription(vo.getGcdDescripcion());
        g.setCreatedOn(dateConverter.convert(vo.getFAlta()));
        return g;
    }
}
