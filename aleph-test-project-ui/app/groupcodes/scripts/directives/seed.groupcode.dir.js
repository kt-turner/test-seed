'use strict';
class GroupcodeDirectiveController{
	
	/*@ngInject*/
	constructor(api, $state,favGroupcodeService){
		this.api = api;
		this.state = $state;
		if (!this._isNew()){
			this._execute();
		}
		this.favGroupcodeService = favGroupcodeService;
	}
	
	_execute() {
		this.api
			.groupcodes
			.get( {groupcodeId:this.id} )
			.$promise
			.then(response => {
				this.groupcode=response;}
			)
	}
	
	_isNew() {
		return this.id === 'new';
	}

	create(form){
		this.api
			.groupcodes
			.save({},this.groupcode)
			.$promise
			.then(response => {
				this._callbackExecute()				
				});
	}
	
	_delete(){		
		this.api
			.groupcodes
			.remove({groupcodeId:this.groupcode.id})
			.$promise
			.then(response => {
				this.state.go('groupcodes')
			});
	}
	
	update(form){
		this.api
			.groupcodes
			.update({groupcodeId:this.id},this.groupcode)
			.$promise
			.then(response => {
				this._callbackExecute()
			});
	}
	
	save(form){
		if(!form.$valid){
			return false
		}
		
		this._getStrategy()
	}
	
	_getStrategy(){
		this._isNew()?this.create():this.update();
	}
	
	_callbackExecute() {
		this.isEdit = false;
		this.state.go('groupcodedetail',{id:this.groupcode.id})
	}
}

export default class GroupcodeDirective{
	
	constructor(){
		this.restrict = 'EA'; //E-Element A-Attribute
		this.replace = false; //Mantiene el tag definido.
		this.scope = {}; //Scope vacío
		this.bindToController = {
				id : '='
		};
		this.controller = GroupcodeDirectiveController;
		this.controllerAs = 'vm'; 
		this.templateUrl = 'groupcodes/scripts/directives/views/seed.groupcode.html';
	}
	
	static directiveFactory(){
		return new GroupcodeDirective();
	}
	
} 